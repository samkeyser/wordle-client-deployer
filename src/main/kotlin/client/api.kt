package client

import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.request.*
import javafx.application.Platform
import kotlinx.coroutines.runBlocking


fun processResponse(response : HttpClient) : Response {
    return Response()
}

fun get(url : String, callback : (Response) -> Unit) {
    Platform.runLater {
        runBlocking {
            val client = HttpClient(CIO)
            val response : HttpClient = client.get("${Config.getURL()}/$url")
            callback(processResponse(response))
        }
    }
}

fun post(url : String, request : HttpRequestBuilder, callback : (Response) -> Unit) {
    Thread {
        Platform.runLater {
            runBlocking {
                val client = HttpClient(CIO)
                request.host = "${Config.getURL()}/$url"
                val response : HttpClient = client.post(request)
                callback(processResponse(response))
            }
        }
    }
}

object Config {
    val productionMode = false
    private const val productionURL = "0.0.0.0"
    val devURL = "0.0.0.0"

    fun getURL() : String {
        return if (productionMode) {
            productionURL
        } else {
            devURL
        }
    }
}

class Response {

}