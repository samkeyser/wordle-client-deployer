package client.core

import client.Response


fun getOfficialWord(callback : (Response) -> Unit) {
    client.get("/api/official-word", callback)
}