/*
* Course: SE2800 - 011
* Spring 2022
*
* Name: Sam Keyser
* Created: 03/28/22
*/
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

//Declare plugins used by gradle, which bundle configuration presets
plugins {
    java
    application //Sets up presets for running the program from gradle (Or intellij...)
    kotlin("jvm") version "1.6.20-RC"
    id("org.jetbrains.dokka") version "1.6.10"
    id("maven-publish")
    id("java-library")
}

group = "msoe.se2800.teamb"
version = "1.0-SNAPSHOT"
val ktor_version = "1.6.8"
repositories {
    mavenCentral() //Look up point for dependencies
    maven {
        url = uri("https://maven.pkg.jetbrains.space/public/p/ktor/eap")
    }
}

dependencies {
    implementation("org.junit.jupiter:junit-jupiter:5.4.2")
    implementation(kotlin("stdlib-jdk8"))
    implementation("io.ktor:ktor-client-core:$ktor_version")
    implementation("io.ktor:ktor-client-cio:$ktor_version")
    implementation("io.ktor:ktor-client-serialization:$ktor_version")
    testImplementation("io.ktor:ktor-client-mock:$ktor_version")
    //implementation("io.ktor:ktor-server-content-negotiation-jvm:$ktor_version")
    //implementation("io.ktor:ktor-serialization-kotlinx-json-jvm:$ktor_version")
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}

//Define resource handling
sourceSets.main.configure {
    resources.srcDirs("src/main/resources").includes.addAll(arrayOf("**/*.fxml", "**/*.css", "**/*.txt", "**/*.png", "**/*.ttf"))
}

//Defines additional settings for select gradle tasks
tasks {
    "test"(Test::class) {
        useJUnitPlatform()
        reports.html.destination = file("$projectDir/reports/")
        reports.junitXml.destination = file("$projectDir/reports/junit")
    }

    jar {
        manifest.attributes["Main-Class"] = "wordle.Main"
        val dependencies = configurations
            .runtimeClasspath
            .get()
            .map(::zipTree) // OR .map { zipTree(it) }
        from(dependencies)
        duplicatesStrategy = DuplicatesStrategy.EXCLUDE
    }
}

publishing {
    publications {
        create<MavenPublication>("wordle-client") {
            groupId = "msoe.se2800.teamb"
            artifactId = "wordle-client"
            version = "1.0"

            from(components["java"])
        }
    }
}

// Define compile settings. Specifically, ensure that we're targeting the correct jdk (1.8)
val compileKotlin: KotlinCompile by tasks
compileKotlin.kotlinOptions {
    jvmTarget = "1.8"
}
val compileTestKotlin: KotlinCompile by tasks
compileTestKotlin.kotlinOptions {
    jvmTarget = "1.8"
}
val compileJava: JavaCompile by tasks
compileJava.targetCompatibility = "1.8"
val compileTestJava: JavaCompile by tasks
compileTestJava.targetCompatibility = "1.8"